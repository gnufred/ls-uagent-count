module gitlab.com/gnufred/ls-uagent-count

go 1.20

require (
	github.com/araddon/dateparse v0.0.0-20210429162001-6b43995a97de
	github.com/mattn/go-sqlite3 v1.14.16
	github.com/petar-dambovaliev/aho-corasick v0.0.0-20211021192214-5ab2d9280aa9
	github.com/schollz/progressbar/v3 v3.13.1
	github.com/spf13/pflag v1.0.5
	gitlab.com/gnufred/nxparse v1.2.0
)

require (
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/mitchellh/colorstring v0.0.0-20190213212951-d06e56a500db // indirect
	github.com/rivo/uniseg v0.4.4 // indirect
	golang.org/x/sys v0.8.0 // indirect
	golang.org/x/term v0.7.0 // indirect
)
