package main

import (
	"log"

	"gitlab.com/gnufred/ls-uagent-count/args"
	"gitlab.com/gnufred/ls-uagent-count/root"
)

var Version = "dev"

func main() {
	Init()
	Run()
}

func Init() {
	args.Init()
}

func Run() {
	a := args.Get()

	_, err := root.ParseStdin(a)
	if err != nil {
		log.Fatal(err)
	}
}
