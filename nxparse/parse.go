package nxparse

import (
	"errors"
	"fmt"
	"net"
	"strings"
	"time"
)

const (
	TimeLayoutIso8601 = time.RFC3339
	TimeLayoutLocal   = "02/Jan/2006:15:04:05 -0700"
)

// Entry parsing errors.
var (
	ErrNilVarName   = errors.New("format variable nil after first position")
	ErrNilSeparator = errors.New("format separator nil before last position")
	ErrParseFailed  = errors.New("parsing log line failed")
	ErrSepNotFound  = errors.New("separator not found")
	ErrVarNotExist  = errors.New("log variable does not exist")
	ErrConverFail   = errors.New("value conversion failed")
	ErrIpConvFail   = errors.New("IP address conversion failed")
)

// An Entry represents a line from the NGINX access log.
// Typically, you'll use Parse() to populate this.
type Entry struct {
	BodyBytesSent      int
	BytesSent          int
	Connection         string
	ConnectionRequests int
	Http               map[string]string
	Msec               int
	Pipe               bool
	RemoteAddr         net.IP
	RemoteUser         string
	Request            string
	RequestLength      int
	RequestTime        int
	Status             int
	TimeIso8601        time.Time
	TimeLocal          time.Time
}

func newEntry(nHeaders uint16) Entry {
	return Entry{
		Http: make(map[string]string, nHeaders),
	}
}

// parseLine does the actual parsing of the log string into structured data.
func parseLine(frmt format, line string) (Entry, error) {
	// Prepare scan.
	frmtLen := len(frmt.Vars)
	ent := newEntry(frmt.NumHttp)

	// Scan each formatVars.
	for i := 0; i < frmtLen; i++ {
		// prepare variables
		name := frmt.Vars[i].Name
		sep := frmt.Vars[i].Separator
		idx := 0

		// If there's a separator, try to find it.
		if sep != "" {
			idx = strings.Index(line, sep)
			if idx < 0 {
				return Entry{}, fmt.Errorf("%s: %w", sep, ErrSepNotFound)
			}
		}

		// If we found the separator, and there's a variable name.
		if idx >= 0 && name != "" {
			value := line[:idx]
			err := assignToEntry(&ent, name, value)
			if err != nil {
				return Entry{}, err
			}

			//truncate line
			start := idx + len(sep)
			line = line[start:]
		}
	}

	return ent, nil
}

// assignToEntry convert an NGINX log variable value to its appropriate GoLang type
// and save it into the entry struct. It blindly overwrites content.
func assignToEntry(ent *Entry, name, val string) error {
	switch name {
	default:
		// first check if the value is an HTTP header
		if name[0:5] == "http_" {
			// the [5:] removes the "http_" from the index
			ent.Http[name[5:]] = val
			return nil
		}
		// return with an error
		return fmt.Errorf("%s: %w", name, ErrVarNotExist)
	case "body_bytes_sent":
	case "bytes_sent":
	case "connection":
	case "connection_requests":
	case "msec":
	case "pipe":
	case "remote_addr":
	case "remote_user":
	case "request":
	case "request_length":
	case "request_time":
	case "status":
	case "time_iso8601":
	case "time_local":
	}
	return nil
}
