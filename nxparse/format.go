package nxparse

import "strings"

// Default `combined` `log_format`.
const (
	Combined = "$remote_addr - $remote_user [$time_local] \"$request\" $status $body_bytes_sent \"$http_referer\" \"$http_user_agent\""
)

// Pre defined formats. They should only be used with formatCache and tests.
var (
	// Default `combined` `log_format`.
	combinedPredef = format{
		Vars: map[int]formatVar{
			0: {"remote_addr", " - "},
			1: {"remote_user", " ["},
			2: {"time_local", "] \""},
			3: {"request", "\" "},
			4: {"status", " "},
			5: {"body_bytes_sent", " \""},
			6: {"http_referer", "\" \""},
			7: {"http_user_agent", "\""},
		},
		NumHttp: 2,
	}
	// Nil value.
	nilPredef = format{
		Vars: map[int]formatVar{
			0: {"", ""},
		},
		NumHttp: 2,
	}
)

// A Format represents the NGINX `log_format` directive.
type format struct {
	Vars    map[int]formatVar
	NumHttp uint16
}

// A FormatVar represents a single variable from the NGINX `log_format` directive.
type formatVar struct {
	Name      string
	Separator string
}

// A varPosiMatrix represents a position matrix for a FormatVar. It has a matrix for
// both the variable and the separator.
type varPosMatrix struct {
	Var posMatrix
	Sep posMatrix
}
type posMatrix struct {
	Beg int
	End int
}

// parseFormat parse the NGNINX `log_format` string into structured data that we can
// use to parse the log line.
//
// To parse to format we need to loop through the string one character at a time and
// understand what's going on.
//
// NGINGX `log_format` identify variables as a string that starts with a '$' followed
// by [a-z_] characters. After that comes the separator, which can include any
// characters.
//
// Two special cases can occur.
// First case: We can have a separator, or no separator, before the first variable.
// This means that the FormatVar[0] could have two zero values.
// Second case: The last variable, has no separator.
func parseFormat(frmtstr string) format {
	frmt := format{Vars: make(map[int]formatVar)}
	frmt.NumHttp = uint16(strings.Count(frmtstr, "http_"))
	pos := varPosMatrix{}

	inVar := false
	lastChar := len(frmtstr) - 1

	// First special case: There's a separator before the first variable.
	if dolIdx := strings.Index(frmtstr, "$"); dolIdx > 0 {
		frmt.Vars[0] = formatVar{"", frmtstr[0:dolIdx]}
	}

	for i, c := range frmtstr {

		// Scan for var name start.
		if c == '$' {
			inVar = true
			pos.Sep.End = i - 1

			// It's not the first variable we encounter.
			if pos.Var.Beg > 0 {
				frmt.appendVar(frmtstr, pos)
			}

			pos.Var.Beg = i + 1
		} else

		// Scan for var name ending.
		if inVar && !isVarNameChar(c) {
			inVar = false
			pos.Var.End = i - 1
			pos.Sep.Beg = i
		}

		// Handle the last character properly.
		if i == lastChar {
			if inVar {
				pos.Var.End = i
				pos.Sep.Beg = i
			}
			pos.Sep.End = i
			frmt.appendVar(frmtstr, pos)
		}
	}

	return frmt
}

// isVarNameChar checks if a character is a valid character for an NGINX `log_format`
// variable. Valid characters [a-z0-9_].
func isVarNameChar(c rune) bool {
	return (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') || c == '_'
}

// Format.appendVar appends a new FormatVar at the end of a FormatVar map
func (f *format) appendVar(frmtstr string, pos varPosMatrix) {
	s := ""
	v := frmtstr[pos.Var.Beg : pos.Var.End+1]

	// Handle second special case: last variable might not have a separator.
	if varHasSep(pos) {
		s = frmtstr[pos.Sep.Beg : pos.Sep.End+1]
	}

	vLen := len(f.Vars)
	f.Vars[vLen] = formatVar{v, s}
}

// varHasSep checks that a variable has separators based on the position matrix. If
// the var end position equals both the separator start and end position, it
// doesn't have a separator.
func varHasSep(pos varPosMatrix) bool {
	return pos.Var.End != pos.Sep.Beg && pos.Var.End != pos.Sep.End
}
