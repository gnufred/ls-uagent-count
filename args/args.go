package args

import (
	"fmt"

	"github.com/spf13/pflag"
)

type Args struct {
	Debug   int
	LogFrmt string
}

var args = Args{}

func Init() {
	if !pflag.Parsed() {
		parse()
		validate()
	}
}

func Get() Args {
	return args
}

func validate() {
	// --debug argument is invalid.
	if args.Debug < 0 || args.Debug > 3 {
		fmt.Printf("Error: debug args must be -v or --debug=[1,3]\n\n")
	}
}

func parse() {
	// Get the current pflag SetFlag
	flags := pflag.CommandLine

	// Debug flags.
	flags.CountP("debug", "v", "Debug: verbosity")

	// Parse and save CLI args.
	args = Args{}
	pflag.Parse()

	debugCnt, _ := flags.GetCount("debug")
	args.Debug = debugCnt
}
