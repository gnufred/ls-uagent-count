package root

import (
	"errors"
	"fmt"
	"io"

	"gitlab.com/gnufred/ls-uagent-count/args"
	"gitlab.com/gnufred/ls-uagent-count/pipe"

	nginx "gitlab.com/gnufred/ls-uagent-count/nxparse"
)

type Parsed struct {
	Ok   uint
	Skip uint
	Err  uint
	Atk  uint
	Bot  uint
}

var (
	ErrNoFormat     = errors.New("no log_format specified")
	ErrNoInputData  = errors.New("no input data")
	ErrDbCreateFail = errors.New("database creation error")
	ErrReadPipe     = errors.New("error while reading piped io.Stdin")
)

func ParseStdin(a args.Args) (Parsed, error) {
	pi := pipe.New()
	par := Parsed{}

	if !pi.IsPiped() {
		return par, ErrNoInputData
	}

	// Parser loop.
	for {
		line, _, err := pi.ReadLine()

		if err == io.EOF {
			break
		}

		if err != nil {
			return par, fmt.Errorf("%s: %w", err, ErrReadPipe)
		}

		nx, err := nginx.Parse(nginx.Combined, line)
		fmt.Println(nx.Http["user_agent"])
	}

	return par, nil
}
